package org.example;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;

public class MessageServiceTest {


    Network network = Mockito.mock(Network.class);

    @Test
    public void messageWasReceived(){
        Mockito.when(network.sendMessage("192.168.13.2","Success")).thenReturn(true);

        MessageService messageService = new MessageService();
        messageService.sendMessage("192.168.13.2","Success");
        assertTrue(messageService.sendMessage("192.168.13.2","Success"));
    }

    @Test
    public void messageFailedOneTime(){
        Mockito.when(network.sendMessage("192.168.13.2","Fail")).thenReturn(false);
        MessageService messageService = new MessageService();
        messageService.sendMessage("192.168.13.2","Fail");
        assertTrue(messageService.sendMessage("192.168.13.2","Success"));
    }

    @Test
    public void messageFailedtwoTimes(){
        Mockito.when(network.sendMessage("192.168.13.2","Fail")).thenReturn(false);
        MessageService messageService = new MessageService();
        messageService.sendMessage("192.168.13.2","Fail");
        assertFalse(messageService.sendMessage("192.168.13.2","Fail"));
    }

}